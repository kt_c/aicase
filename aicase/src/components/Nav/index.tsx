import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import './index.scss'
import AICase from '../../assets/imgs/logo.svg'

const items: MenuProps['items'] = [
  {
    label: '主页',
    key: 'home',
  },
  {
    label: '人脸识别',
    key: 'faceRecognize',
  },
  {
    label: '图像识别',
    key: 'imgRecognize',
  }, {
    label: '手势识别',
    key: 'gestureRecognize',
  }, {
    label: '语音识别',
    key: 'voiceRecognize',
  }, {
    label: '自然语言处理',
    key: 'naturalLanguage',
  },
  {
    label: '深度学习',
    key: 'deepLearning',
  },
  {
    label: '其他应用',
    key: 'otherApp',
  },
];

export default function Nav() {
  const navigate = useNavigate()
  const [current, setCurrent] = useState('home');

  const onClick: MenuProps['onClick'] = (e) => {
    setCurrent(e.key);
    navigate(`/${e.key}`)
  };

  const toHome = () => {
    setCurrent('home');
    navigate(`/Home`)
  }
  return (
    <div className='Nav'>
      <div className="logo" onClick={toHome}>
        <img src={AICase} alt="AICase" width='40px' />
        <h2 className="label">
          AICase
        </h2>
      </div>
      <div className="menu">
        <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />
      </div>
    </div>
  )
}
