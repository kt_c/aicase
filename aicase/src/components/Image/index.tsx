import React, { useState } from 'react';
import { Skeleton } from 'antd';
interface ImageComponentProps {
    src: string;
    placeholder?: string;
}

const ImageComponent: React.FC<ImageComponentProps> = ({ src, placeholder }) => {
    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);

    const handleImageLoad = () => {
        setIsLoading(false);
    };

    const handleImageError = () => {
        setIsLoading(false);
        setIsError(true);
    };

    return (
        <>
            {isLoading && <Skeleton.Image active={true} />
            }
            {isError && <div>Error loading image.</div>}
            <img
                src={src}
                onLoad={handleImageLoad}
                onError={handleImageError}
                style={{ display: isLoading || isError ? 'none' : 'block' }}
                alt=""
            />
        </>
    );
};

export default ImageComponent;
