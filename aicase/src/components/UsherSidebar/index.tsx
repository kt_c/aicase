import { useContext, useState } from 'react'
import './index.scss'
import UsherUploadList from './UsherUploadList'

import { AppContext } from '../../App'

import { FolderOutlined } from '@ant-design/icons'
import { Button } from 'antd'

type props = { // 参数类型
    setSpotResult: React.Dispatch<React.SetStateAction<string>>, // 人脸检测结果
    module: string, // 模块 用于建立不同的本地存储
  }

export default function UsherSidebar(props: props) {

    const { setProcessSwitch } = useContext(AppContext)!;
    const { setSpotResult, module } = props // 人脸识别设置 modal设置

    const [trained, setTrained] = useState(false); // 未训练

    const modelTraining = () => { // 模型训练
        setProcessSwitch ? setProcessSwitch(true) : '';
        setTrained(true);
    }

  return (
    <div className='left_sidebar Usher_Sidebar'>
      <div className="gallery">
        <div className="head">
            <div className="head_left">
                <FolderOutlined />
                图库
            </div>
            <div className="head_right">
                <Button size={'small'} onClick={modelTraining}>人脸检测模型训练</Button>
            </div>
        </div>
        <div className="body">
          <UsherUploadList setSpotResult={setSpotResult} trained={trained} module={module}/>
        </div>
      </div>
    </div>
  )
}
