import { UserOutlined, FolderOutlined, PlusOutlined } from '@ant-design/icons'
import { Modal, Upload, Button, Space } from 'antd';
import type { RcFile, UploadProps } from 'antd/es/upload';
import type { UploadFile } from 'antd/es/upload/interface';
import UploadList from './UploadList';
import React, { useState, useContext } from 'react'
import './index.scss'
import { getBase64 } from '../../utils/dataChange'
import { AppContext } from '../../App';

const uploadButton = ( // 上传按钮
  <div className='uploadBtn'>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>上传</div>
  </div>
);

type props = { // 参数类型
  setSpotResult: React.Dispatch<React.SetStateAction<string>>, // 人脸识别结果
  setIsModalOpen?: React.Dispatch<React.SetStateAction<boolean>> | undefined, // 门选择左右的开关 可传可不传
  module: string, // 模块 用于建立不同的本地存储
  text: string,
}

export default function Sidebar(props: props) { // 侧边栏

  const { setProcessSwitch } = useContext(AppContext)!;
  const { setSpotResult, setIsModalOpen, module, text } = props // 人脸识别设置 modal设置

  const local_presetFileList = JSON.parse(localStorage.getItem(`AICase_preset_fileList_${module}`) as string) // 本地获取
  const local_trainFileList = JSON.parse(localStorage.getItem(`AICase_train_fileList_${module}`) as string)

  const [previewOpen, setPreviewOpen] = useState(false); // 预览开关
  const [previewImage, setPreviewImage] = useState(''); // 预览照片
  const [previewTitle, setPreviewTitle] = useState(''); // 预览标题
  const [presetFileList, setPresetFileList] = useState<UploadFile[]>(local_presetFileList); // 预设文件列表
  const [trainFileList, setTrainFileList] = useState<UploadFile[]>(local_trainFileList); // 已训练文件列表

  const handleCancel = () => setPreviewOpen(false); // 撤销

  const handlePreview = async (file: UploadFile) => { // 预览

    setPreviewImage(file.thumbUrl as string);
    setPreviewOpen(true);
    setPreviewTitle(file.name || file.url!.substring(file.url!.lastIndexOf('/') + 1));
  };

  const handlePresetChange: UploadProps['onChange'] = ({ fileList: newFileList }) => { // 预设的图片变化处理
    setPresetFileList(newFileList);
    localStorage.setItem(`AICase_preset_fileList_${module}`, JSON.stringify(newFileList))
  }

  const modelTraining = () => { // 模型训练
    setProcessSwitch ? setProcessSwitch(true) : '';
    setTrainFileList(presetFileList)
    localStorage.setItem(`AICase_train_fileList_${module}`, JSON.stringify(presetFileList))
  }

  return (
    <div className="left_sidebar">
      <div className="info_preset">
        <div className="head">
          <div className="head_left">
            <UserOutlined />{text}
          </div>
          <div className="head_right">
            <Button size={'small'} onClick={modelTraining}>模型训练</Button>
          </div>
        </div>
        <div className="body">
          <Upload
            listType="picture-card"
            fileList={presetFileList}
            onPreview={handlePreview}
            onChange={handlePresetChange}
            beforeUpload={() => false}
          >
            {presetFileList && presetFileList.length >= 30 ? null : uploadButton}
          </Upload>
          <Modal open={previewOpen} title={previewTitle} footer={null} onCancel={handleCancel}>
            <img alt="img" style={{ width: '100%' }} src={previewImage} />
          </Modal>
        </div>
      </div>
      <div className="gallery">
        <div className="head">
          <div className="head_left">
            <FolderOutlined />
            图库
          </div>
        </div>
        <div className="body">
          <UploadList trainFileList={trainFileList} setSpotResult={setSpotResult} setIsModalOpen={setIsModalOpen} module={module}/>
        </div>
      </div>
    </div>
  )
}
