import React,{ useState, useContext, useEffect } from 'react'

import { PlusOutlined, CheckOutlined, DeleteOutlined } from '@ant-design/icons'
import type { UploadFile } from 'antd/es/upload/interface';
import axios from 'axios'

import { getBase64 } from '../../../utils/dataChange'
import { removeBase64Prefix } from '../../../utils/stringFunc'
import { AppContext } from '../../../App'
import './index.scss'

type galleryFile = { // 图库文件类型
  name: string,
  thumbUrl: string,
  lastModified: number
}

const uploadButton = ( // 上传按钮
  <div className='uploadBtn'>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>上传</div>
  </div>
);

type props = {
  trainFileList: UploadFile[], 
  setSpotResult: React.Dispatch<React.SetStateAction<string>>, 
  setIsModalOpen?: React.Dispatch<React.SetStateAction<boolean>> | undefined,
  module: string;
}
export default function UploadList(props: props) { // 文件上传列表

  const { setLoading } = useContext(AppContext)!

  const { trainFileList, setSpotResult, setIsModalOpen, module } = props // 预设文件列表 人脸识别结果 modal开关设置

  const local_galleryFileList = JSON.parse(localStorage.getItem(`AICase_gallery_fileList_${module}`) as string) // 本地图库
  const [galleryFileList, setGalleryFileList] = useState<galleryFile[]>(local_galleryFileList) // 图库文件列表

  const showModal = () => { // 信息展示
    setIsModalOpen ? setIsModalOpen(true) : '';
  };

    const handleGalleryChange = (e: any) => { // 文件选择
        getBase64(e.target.files[0]).then((res)=>{
        const fileList = galleryFileList ? [...galleryFileList] : []; // 状态监听的是引用 所以想要监听数组的变化得深拷贝
        fileList.push({
          thumbUrl: res,
          name: e.target.files[0].name,
          lastModified: e.target.files[0].lastModified
        })
        setGalleryFileList(fileList)
        localStorage.setItem(`AICase_gallery_fileList_${module}`, JSON.stringify(fileList))
      })
    }
  
    const handleGalleryClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => { // 图库点击处理
      const target = e.target as HTMLDivElement
      const currentTarget = e.currentTarget as HTMLDivElement
      const img = currentTarget.querySelector('img') as HTMLImageElement
  
      if(target.classList.value.includes("deleteIcon")){ // 删除
        const result = galleryFileList.filter((obj:galleryFile)  => obj.lastModified.toString() !== img.getAttribute('data-lastmodified'))
        setGalleryFileList(result)
        localStorage.setItem(`AICase_gallery_fileList_${module}`, JSON.stringify(result))
      }
      else if(target.classList.value.includes("checkOutIcon")){ // 确认
        // 获取自定义属性值并比较
        const galleryImg = removeBase64Prefix(img.src)
        const collectionImageItems = trainFileList.map((value) => removeBase64Prefix(value.thumbUrl))
        
        setLoading(true)
        axios({
          method: 'post',
          url: `http://127.0.0.1:7777/face`,
          data: {
            collectionImageItems: collectionImageItems,
            compareImageItem: galleryImg,
          },
        }).then((res)=>{          
          setSpotResult(res.data.status ? 'true' : 'false');
          console.log(res.data.msg);
          showModal()
          setLoading(false)
        },(err)=>{
          setSpotResult('false');
          showModal()
          setLoading(false)
          console.log(err);
        })
     } 
  }

  return (
    <div className="upload_list">
      {galleryFileList ? galleryFileList.map((value: galleryFile, index: number) => {
        return (
          <div className="upload_container" onClick={(e) => handleGalleryClick(e)} key={`upload-container-${index}`}>
            <div className="upload upload_active">
              <img src={value.thumbUrl} alt="" width='102px' height='102px' className='uploadImg' data-name={value.name} data-lastmodified={value.lastModified} />
              <div className="mask">
                <CheckOutlined className='icon checkOutIcon' />
                <DeleteOutlined className='icon deleteIcon' />
              </div>
            </div>
          </div>)
      }) : ''}
      <div className="upload_container">
        <div className="upload">
          <input type="file" accept='.png, .svg, .jpg, .jpeg, .jfif' onChange={handleGalleryChange} />
          {uploadButton}
        </div>
      </div>
    </div>
  )
}
