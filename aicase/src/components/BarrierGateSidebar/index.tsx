import { useContext, useState } from 'react'
import BarrierGateUploadList from './BarrierGateUploadList'

import { AppContext } from '../../App'

import { FolderOutlined } from '@ant-design/icons'
import { Button, Modal, Upload, UploadFile } from 'antd'

type props = { // 参数类型
    setSpotResult: React.Dispatch<React.SetStateAction<string>>, // 人脸检测结果
    module: string, // 模块 用于建立不同的本地存储
  }

export default function BarrierGateSidebar(props: props) {

    const { setProcessSwitch } = useContext(AppContext)!;
    const { setSpotResult, module } = props // 人脸识别设置 modal设置

    const [trained, setTrained] = useState(false); // 未训练

    const modelTraining = () => { // 模型训练
        setProcessSwitch ? setProcessSwitch(true) : '';
        setTrained(true);
    }

    const local_collFileList = JSON.parse(localStorage.getItem(`AICase_coll_fileList_${module}`) as string) // 本地获取人脸采集库
  
    const [previewOpen, setPreviewOpen] = useState(false); // 预览开关
    const [previewImage, setPreviewImage] = useState(''); // 预览照片
    const [previewTitle, setPreviewTitle] = useState(''); // 预览标题

    const [collFileList, setCollFileList] = useState<UploadFile[]>(local_collFileList); // 人脸采集文件列表
  
    const handleCancel = () => setPreviewOpen(false); // 撤销
  
    const handlePreview = async (file: UploadFile) => { // 预览
      setPreviewImage(file.thumbUrl as string);
      setPreviewOpen(true);
      setPreviewTitle(file.name || file.url!.substring(file.url!.lastIndexOf('/') + 1));
    };

    const handleRemove = (file: UploadFile)=> { // 删除
      console.log('file->', file.uid);
      const result = collFileList.filter((obj:UploadFile)  => obj.uid.toString() !== file.uid)
      setCollFileList(result)
      localStorage.setItem(`AICase_coll_fileList_${module}`, JSON.stringify(result))
    }

  return (
    <div className='left_sidebar BarrierGate_Sidebar'>
      <div className="gallery">
        <div className="head">
            <div className="head_left">
                <FolderOutlined />
                图库
            </div>
            <div className="head_right">
                <Button size={'small'} onClick={modelTraining}>人脸检测模型训练</Button>
            </div>
        </div>
        <div className="body">
          <BarrierGateUploadList setSpotResult={setSpotResult} trained={trained} module={module} collFileList={collFileList} setCollFileList={setCollFileList}/>
        </div>
      </div>
      <div className="gallery">
        <div className="head">
            <div className="head_left">
                <FolderOutlined />
                人脸采集库
            </div>
        </div>
        <div className="body">
          <Upload
              listType="picture-card"
              fileList={collFileList}
              onPreview={handlePreview}
              beforeUpload={() => false}
              onRemove={handleRemove}
            >
              {collFileList && null}
            </Upload>
            <Modal open={previewOpen} title={previewTitle} footer={null} onCancel={handleCancel}>
              <img alt="img" style={{ width: '100%' }} src={previewImage} />
            </Modal>
        </div>
      </div>
    </div>
  )
}
