import { SnippetsOutlined, FolderOutlined, PlusOutlined } from '@ant-design/icons'
import { Modal, Upload, Button, QRCode } from 'antd';
import type { UploadProps } from 'antd/es/upload';
import type { UploadFile } from 'antd/es/upload/interface';
import React, { useState, useContext } from 'react'
import CashierUploadList from './CashierUploadList';
import { CashierContext } from '../../pages/FaceRecognize/Cashier';

const uploadButton = ( // 上传按钮
  <div className='uploadBtn'>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>上传</div>
  </div>
);

type props = { // 参数类型
  module: string, // 模块 用于建立不同的本地存储
  text: string,
}

export default function CashierSIdebar(props: props) {

    const { setSpotResult } = useContext(CashierContext)!
    const { module, text } = props // 人脸识别设置 modal设置
  
    const local_presetFileList = JSON.parse(localStorage.getItem(`AICase_preset_fileList_${module}`) as string) // 本地获取
  
    const [previewOpen, setPreviewOpen] = useState(false); // 预览开关
    const [previewImage, setPreviewImage] = useState(''); // 预览照片
    const [previewTitle, setPreviewTitle] = useState(''); // 预览标题
    const [presetFileList, setPresetFileList] = useState<UploadFile[]>(local_presetFileList); // 预设文件列表
  
    const handleCancel = () => setPreviewOpen(false); // 撤销
  
    const handlePreview = async (file: UploadFile) => { // 预览
      setPreviewImage(file.thumbUrl as string);
      setPreviewOpen(true);
      setPreviewTitle(file.name || file.url!.substring(file.url!.lastIndexOf('/') + 1));
    };
  
    const handlePresetChange: UploadProps['onChange'] = ({ fileList: newFileList }) => { // 预设的图片变化处理
      setPresetFileList(newFileList);
      localStorage.setItem(`AICase_preset_fileList_${module}`, JSON.stringify(newFileList))
    }

    const handleGoods = () => { // 商品扫描处理
        setSpotResult('qrcode');
    }

  return (
    <div className="left_sidebar Cashier_sidebar">
      <div className="gallery">
        <div className="head">
          <div className="head_left"><FolderOutlined />{text}</div>
          <div className="head_right">
            <Button size={'small'} onClick={handleGoods}>商品扫描</Button>
          </div>
        </div>
        <div className="body">
            <Upload
                listType="picture-card"
                fileList={presetFileList}
                onPreview={handlePreview}
                onChange={handlePresetChange}
                beforeUpload={() => false}
            >
                {presetFileList && presetFileList.length >= 30 ? null : uploadButton}
            </Upload>
            <Modal open={previewOpen} title={previewTitle} footer={null} onCancel={handleCancel}>
                <img alt="img" style={{ width: '100%' }} src={previewImage} />
            </Modal>
        </div>
      </div>
      <div className="gallery">
        <div className="head">
          <div className="head_left"><SnippetsOutlined/>图库</div>
        </div>
        <div className="body">
            <CashierUploadList module={module}/>
        </div>
      </div>
    </div>
  )
}
