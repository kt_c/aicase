import React,{ useState, useContext } from 'react'

import { PlusOutlined, CheckOutlined, DeleteOutlined } from '@ant-design/icons'
import { message } from 'antd'

import { getBase64 } from '../../../utils/dataChange'
import { removeStringAfterDash } from '../../../utils/stringFunc'
import { AppContext } from '../../../App'
import { CashierContext } from '../../../pages/FaceRecognize/Cashier'

type galleryFile = { // 图库文件类型
    name: string,
    thumbUrl: string,
    lastModified: number
}
  
const uploadButton = ( // 上传按钮
    <div className='uploadBtn'>
        <PlusOutlined />
        <div style={{ marginTop: 8 }}>上传</div>
    </div>
);

type props = {
    module: string,
}

export default function CashierUploadList(props: props) {

    const [messageApi, contextHolder] = message.useMessage(); // antd 全局提示的钩子

    const { setLoading } = useContext(AppContext)!
    const { spotResult, setSpotResult } = useContext(CashierContext)!

    const { module } = props // 预设文件列表 人脸识别结果 modal开关设置
  
    const local_galleryFileList = JSON.parse(localStorage.getItem(`AICase_gallery_fileList_${module}`) as string) // 本地图库
    const [galleryFileList, setGalleryFileList] = useState<galleryFile[]>(local_galleryFileList) // 图库文件列表
  
    const handleGalleryChange = (e: any) => { // 文件选择
        getBase64(e.target.files[0]).then((res)=>{
            const fileList = galleryFileList ? [...galleryFileList] : []; // 状态监听的是引用 所以想要监听数组的变化得深拷贝
            fileList.push({
                thumbUrl: res,
                name: e.target.files[0].name,
                lastModified: e.target.files[0].lastModified
            })
            setGalleryFileList(fileList)
            localStorage.setItem(`AICase_gallery_fileList_${module}`, JSON.stringify(fileList))
        })
    }

    const handleGalleryClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => { // 图库点击处理
        const target = e.target as HTMLDivElement
        const currentTarget = e.currentTarget as HTMLDivElement
        const img = currentTarget.querySelector('img') as HTMLImageElement
    
        if(target.classList.value.includes("deleteIcon")){ // 删除
          const result = galleryFileList.filter((obj:galleryFile)  => obj.lastModified.toString() !== img.getAttribute('data-lastmodified'))
          setGalleryFileList(result)
          localStorage.setItem(`AICase_gallery_fileList_${module}`, JSON.stringify(result))
        }
        else if(target.classList.value.includes("checkOutIcon")){ // 确认
            setLoading(true);
            const str = removeStringAfterDash(img.getAttribute('data-name'));
            if(spotResult === 'qrcode'){
                if(str === '二维码') {
                    setSpotResult('true');
                }
                else {
                    messageApi.open({
                        type: 'warning',
                        content: '请出示二维码',
                    });
                }
            }
            else {
                messageApi.open({
                    type: 'warning',
                    content: '请先进行商品扫描',
                });
            }
            setLoading(false);
       } 
    }

  return (
    <div className="upload_list">
        {contextHolder}
        {galleryFileList ? galleryFileList.map((value: galleryFile, index: number) => {
            return (
                <div className="upload_container" onClick={(e) => handleGalleryClick(e)} key={`upload-container-${index}`}>
                <div className="upload upload_active">
                    <img src={value.thumbUrl} alt="" width='102px' height='102px' className='uploadImg' data-name={value.name} data-lastmodified={value.lastModified} />
                    <div className="mask">
                    <CheckOutlined className='icon checkOutIcon' />
                    <DeleteOutlined className='icon deleteIcon' />
                    </div>
                </div>
                </div>)
        }) : ''}
        <div className="upload_container">
        <div className="upload">
            <input type="file" accept='.png, .svg, .jpg, .jpeg, .jfif' onChange={handleGalleryChange} />
            {uploadButton}
        </div>
        </div>
  </div>
  )
}
