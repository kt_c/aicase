import type { RcFile } from 'antd/es/upload';

export const getBase64 = (file: RcFile): Promise<string> => // 获取base64
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result as string);
    reader.onerror = (error) => reject(error);
});