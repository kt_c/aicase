import React, { useEffect, useRef } from 'react'
import './index.scss'

export default function HoverWord(props:{text:string, fontSize: string, color: string}) {

    const span = (text: string, index: number) => { // 每一个单词都将生成一个span
        const node = document.createElement('span')
        node.textContent = text
        // 为每一个span加上自定义属性名与属性值
        node.style.setProperty('--index', `${index}`)
        return node
      }
      
      const byWord = (text: string) =>{ // 单词分隔
        return text.split(' ').map(span)
      }
      
      const {matches:motionOK} = window.matchMedia( // 确认媒体响应是否正常运作
        '(prefers-reduced-motion: no-preference)'
      )
    
      useEffect(()=>{
        if (motionOK) {
          const splitTargets = document.querySelectorAll('[split-by]')
    
          splitTargets.forEach(node => { // 遍历splitTargets
    
            const regex = /<(.*?)>/g; // 匹配字符串中的标签
            const tags = node.innerHTML.match(regex);
    
            if(!tags){ // 判断是否是第一次进来
              let nodes = byWord(node.innerHTML)
              if (nodes){ // 将文本替换成单词span
                node.firstChild?.replaceWith(...nodes)
              }
            }
          })
        }

        const title = document.querySelector('.HoverWord div') as HTMLElement
        if(title){
            title.style.fontSize = `${props?.fontSize}`
            title.style.color = `${props?.color}`
        }
      },[])

  return (
    <div className='HoverWord'>
      <div split-by="word" word-animation="hover">{props?.text}</div>
    </div>
  )
}
