const Sleep = (ms: number) => { // 休眠
    return new Promise(resolve => setTimeout(resolve, ms))
  }
export default Sleep