const fs = require('fs')
const path = require('path')
const currentFilePath = __filename
const parentDirPath = path.join(currentFilePath, '..', '..') // src路径 
const Myreplace = (str, list) => {
  let temp = str
  if (list && str){
      list.map((v,i)=>{
        temp = temp.replace(new RegExp(v.replace(/[/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), '');
      })
  }
  return temp
}
const projectPaths = ['1', '2', '3', '4', '5', '6', '7']
const importArr = []
let otherText = `const getAllimages = (type) => {
  // 在所有图片都成功导入后，可以在这里使用导入的图片
  let importedImages = []
  switch (type) {`
for (let i = 0; i < projectPaths.length; i++) {
  const imageFolderPath = path.resolve(parentDirPath, `assets/imgs/projectFace/${projectPaths[i]}`) // 上级的上级文件的路径
  const imageFiles = fs.readdirSync(imageFolderPath)
  otherText += ` case ${i}:
             importedImages = [${imageFiles.map((name, number) => {
                return (`project${i}${number}`)
             })}]
             break;`
  for (let j = 0; j < imageFiles.length; j++) {
    const Path = path.join(imageFolderPath, imageFiles[j])
    importArr.push(`import project${i}${j} from '${Path}';`)
  }
}

otherText += `     default:
break;
}
return importedImages
}
export default getAllimages`
// 生成js文件的位置
const outputFilePath = path.resolve(parentDirPath, 'pages', 'Project', 'imageList.js')

// 读取原始文件的内容
// const originalContent = fs.readFileSync(outputFilePath, 'utf8')

// 将要写入的新内容与原始文件的内容进行拼接
const combinedContent = `${Myreplace(JSON.stringify(importArr), ['[', ']', '"', ','])}` + otherText

// 将拼接后的内容写入文件
fs.writeFileSync(outputFilePath, combinedContent)
