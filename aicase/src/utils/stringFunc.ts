const removeStringAfterDash = (str: string | null): string => { // 截取字符串中-前面的内容
    if (str) {
      const parts = str.split('-');
      return parts[0];
    } else return ''
  }

  const removeBase64Prefix = (str: string | null | undefined): string => { // 去除base64前缀
    if (str) {
      return str?.replace(/^data:image\/\w+;base64,/, '')
    }
    else return ''
  }

  export { removeStringAfterDash, removeBase64Prefix }