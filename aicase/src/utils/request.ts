import axios from "axios";

export const postRequest = (url: string, data: any) => {
    return new Promise((success, reject) => {
        axios({
            method: 'post',
            headers: {
                "Content-Type": 'application/json'
            },
          url: url,
          data: data,
        }).then((res)=>{
            success(res.data)
        }, (err)=>{
            reject(err)
        })
    })
}