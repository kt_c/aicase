import Home from "../pages/Home";
import Project from '../pages/Project'
import Turnstile from "../pages/FaceRecognize/Turnstile";
import Door from "../pages/FaceRecognize/Door";
import BarrierGate from "../pages/FaceRecognize/BarrierGate";
import Usher from "../pages/FaceRecognize/Usher";
import CheckIn from "../pages/FaceRecognize/CheckIn";
import Cashier from "../pages/FaceRecognize/Cashier";
import Police from "../pages/FaceRecognize/Police";
import Patrol from "../pages/FaceRecognize/Patrol";
import ACGANImg from "../pages/DeepLearning/ACGANImg";
import Captcha from "../pages/DeepLearning/Captcha";
import GANImg from "../pages/DeepLearning/GANImg";
import LicensePlate from "../pages/DeepLearning/LicensePlate";
import { Navigate } from "react-router-dom";

const routes = [
    // 主页
    {
        path: '/Home',
        element: <Home />,
    },
    // 项目页
    //人脸识别
    {
        path: '/faceRecognize',
        element: <Project />,
        children: [
            {
                path: 'Turnstile',
                element: <Turnstile />
            },
            {
                path: 'Door',
                element: <Door />
            },
            {
                path: 'BarrierGate',
                element: <BarrierGate />
            },
            {
                path: 'Usher',
                element: <Usher />
            },
            {
                path: 'CheckIn',
                element: <CheckIn/>
            },
            {
                path: 'Cashier',
                element: <Cashier/>
            },
            {
                path: 'Police',
                element: <Police/>
            },
            {
                path: 'Patrol',
                element: <Patrol/>
            }
        ]
    },
    //图像识别
    {
        path: '/imgRecognize',
        element: <Project />,
        children: []
    },
    //手势识别
    {
        path: '/gestureRecognize',
        element: <Project />,
        children: []

    },
    //语音识别
    {
        path: '/voiceRecognize',
        element: <Project />,
        children: []

    },
    //自然语言处理
    {
        path: '/naturalLanguage',
        element: <Project />,
        children: []

    },
    //深度学习
    {
        path: '/deepLearning',
        element: <Project />,
        children: [
            {
                path: 'licensePlate',
                element: <LicensePlate />
            },
            {
                path: 'ACGANimg',
                element: <ACGANImg />
            },
            {
                path: 'captcha',
                element: <Captcha />
            },
            {
                path: 'GANimg',
                element: <GANImg />
            },
        ]

    },
    //其他应用
    {
        path: '/otherApp',
        element: <Project />,
        children: []

    },

    {
        path: '/',
        element: <Navigate to='/Home' />,
    },
]

export default routes