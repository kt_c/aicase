import { useState, useEffect } from 'react'
import '../index.scss'
import UsherSidebar from '../../../components/UsherSidebar';
import Sleep from '../../../utils/sleep';
import Usher_close from '../../../assets/imgs/faceRecognize/Usher/Usher_close.png'
import Usher_open from '../../../assets/imgs/faceRecognize/Usher/Usher_open.gif'

export default function Usher() {

    const [spotResult, setSpotResult] = useState('standby'); // 人脸检测结果

    const aisleHandle = async () => {
      switch(spotResult){
        case 'standby':
          break;
        case 'false':
          await Sleep(2000);
          setSpotResult('standby');
          break;
        case 'true':  
          await Sleep(4000);
          setSpotResult('standby');
      }
    }
  
    useEffect(()=>{
      aisleHandle();
    },[spotResult]);
  
    return (
      <div className='Usher'>
        <UsherSidebar setSpotResult={setSpotResult} module='Usher'/>
        <div className="Usher_scenes scenes">
          <div className="Usher_img img">
            {
                spotResult === 'standby' ? <img src={Usher_close} alt="" className="Usher_content content"/> : 
                spotResult === 'true' ? <img src={Usher_open} alt="" className="Usher_content content"/> : 
                                    <img src={Usher_close} alt="" className="Usher_content content"/>
            }
          </div>
        </div>
      </div>
    )
}
