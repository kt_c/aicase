import { Modal, Button, Space } from 'antd';
import { useEffect, useState } from 'react'
import './index.scss'
import Sleep from '../../../utils/sleep';

import Sidebar from '../../../components/Sidebar';
import turnstile_bg from '../../../assets/imgs/faceRecognize/turnstile/turnstile_bg.png'
import turnstile_turnstile from '../../../assets/imgs/faceRecognize/turnstile/turnstile_turnstile.png'
import turnstile_left1 from '../../../assets/imgs/faceRecognize/turnstile/turnstile_left1.png'
import turnstile_left2 from '../../../assets/imgs/faceRecognize/turnstile/turnstile_left2.png'
import turnstile_right1 from '../../../assets/imgs/faceRecognize/turnstile/turnstile_right1.png'
import turnstile_right2 from '../../../assets/imgs/faceRecognize/turnstile/turnstile_right2.png'
import turnstile_standby_left from '../../../assets/imgs/faceRecognize/turnstile/turnstile_standby_left.png'
import turnstile_standby_right from '../../../assets/imgs/faceRecognize/turnstile/turnstile_standby_right.png'
import turnstile_green_left from '../../../assets/imgs/faceRecognize/turnstile/turnstile_green_left.png'
import turnstile_green_right from '../../../assets/imgs/faceRecognize/turnstile/turnstile_green_right.png'
import turnstile_red_left from '../../../assets/imgs/faceRecognize/turnstile/turnstile_red_left.png'
import turnstile_red_right from '../../../assets/imgs/faceRecognize/turnstile/turnstile_red_right.png'



export default function Turnstile() { // 闸机

  const [isModalOpen, setIsModalOpen] = useState(false); // modal开关
  const [spotResult, setSpotResult] = useState('standby'); // 照片识别结果
  const [pass, setPass] = useState({ // 通道开关状态
    left: 'standby', // standby allow fail
    right: 'standby'
  })

  const aislehandle = async (left: Element | null, right: Element | null, select: 'right' | 'left') => { // 通道处理
    setIsModalOpen(false);
    const pass_copy1 = { ...pass }

    switch (spotResult) { // 照片是否已训练
      case 'true':
        pass_copy1[select] = 'allow';
        setPass(pass_copy1)
        left?.getAttribute('turnstile-left') === 'open1' ? left?.setAttribute('turnstile-left', 'open2') : left?.setAttribute('turnstile-left', 'open1')
        right?.getAttribute('turnstile-right') === 'open1' ? right?.setAttribute('turnstile-right', 'open2') : right?.setAttribute('turnstile-right', 'open1')

        await Sleep(3000)

        const pass_copy2 = { ...pass }
        pass_copy2[select] = 'standby';
        setPass(pass_copy2)
        setSpotResult('standby')

        break;
      case 'false':
        pass_copy1[select] = 'fail';
        setPass(pass_copy1)

        await Sleep(3000)

        const pass_copy3 = { ...pass }
        pass_copy3[select] = 'standby';
        setPass(pass_copy3)
        setSpotResult('standby')
        break;
    }
  }

  const handleRight = () => { // 走右边通道
    const left = document.querySelector('.turnstile_right1 img')
    const right = document.querySelector('.turnstile_right2 img')

    aislehandle(left, right, 'right')

  };

  const handleLeft = () => { // 走左边通道
    const left = document.querySelector('.turnstile_left2 img')
    const right = document.querySelector('.turnstile_left1 img')

    aislehandle(left, right, 'left')
  };

  return (
      <div className='Turnstile'>
        <Modal
          title="通道选择"
          open={isModalOpen}
          onOk={handleRight}
          onCancel={handleLeft}
          footer={null}>
          <Space className='modal_body'>
            <Button key='leftBtn' onClick={handleLeft} className='modal_body_btn'>左边</Button>
            <Button key='rightBtn' onClick={handleRight} className='modal_body_btn'>右边</Button>
          </Space>
        </Modal>
        <Sidebar setSpotResult={setSpotResult} setIsModalOpen={setIsModalOpen} module='Turnstile' text='人脸信息预设'/>
        <div className="turnstile_scenes">
          <div className="turnstile_img">
            <img src={turnstile_bg} alt="" className='turnstile_bg' />
            <img src={turnstile_turnstile} alt="" className='turnstile_turnstile' />
            <div className='turnstile_left1' ><img src={turnstile_left1} alt="" /></div>
            <div className='turnstile_left2'><img src={turnstile_left2} alt="" /></div>
            <div className='turnstile_right1'><img src={turnstile_right1} alt="" /></div>
            <div className='turnstile_right2'><img src={turnstile_right2} alt="" /></div>
            {
              pass.left === 'standby' ? <div className="turnstile_tip_left"><img src={turnstile_standby_left} alt="" /></div> :
                pass.left === 'allow' ? <div className="turnstile_tip_left"><img src={turnstile_green_left} alt="" /></div> :
                  pass.left === 'fail' ? <div className="turnstile_tip_left"><img src={turnstile_red_left} alt="" /></div> : ''
            }
            {
              pass.right === 'standby' ? <div className="turnstile_tip_right"><img src={turnstile_standby_right} alt="" /></div> :
                pass.right === 'allow' ? <div className="turnstile_tip_right"><img src={turnstile_green_right} alt="" /></div> :
                  pass.right === 'fail' ? <div className="turnstile_tip_right"><img src={turnstile_red_right} alt="" /></div> : ''
            }
          </div>
        </div>
      </div>
  )
}
