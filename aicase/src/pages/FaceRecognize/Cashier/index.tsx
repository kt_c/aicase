import { useState, useEffect, createContext } from 'react'
import '../index.scss'
import Sleep from '../../../utils/sleep';
import CashierSidebar from '../../../components/CashierSidebar';
import Cashier_wait from '../../../assets/imgs/faceRecognize/Cashier/Cashier_wait.png'
import Cashier_qrcode from '../../../assets/imgs/faceRecognize/Cashier/Cashier_qrcode.png'
import Cashier_success from '../../../assets/imgs/faceRecognize/Cashier/Cashier_success.png'

export const CashierContext = createContext<{
  spotResult: string, // 状态
  setSpotResult: React.Dispatch<React.SetStateAction<string>>, // 状态
} | null>(null)

export default function Cashier() {

    const [spotResult, setSpotResult] = useState('standby'); // 人脸检测结果

    const aisleHandle = async () => {
      switch(spotResult){
        case 'standby':
          break;
        case 'true':  
          await Sleep(2000);
          setSpotResult('standby');
      }
    }
  
    useEffect(()=>{
      aisleHandle();
      console.log(spotResult);
      
    },[spotResult]);

  return (
    <CashierContext.Provider value={{ spotResult: spotResult, setSpotResult: setSpotResult}}>
      <div className='Cashier'>
          <CashierSidebar module='Cashier' text='购物车'/>
          <div className="Cashier_scenes scenes">
          <div className="Cashier_img img">
              {
                  spotResult === 'standby' ? <img src={Cashier_wait} alt="" className="Cashier_content content"/> : 
                  spotResult === 'qrcode' ? <img src={Cashier_qrcode} alt="" className="Cashier_content content"/> : 
                  spotResult === 'true' ? <img src={Cashier_success} alt="" className="Cashier_content content"/> : ''
              }
          </div>
          </div>
      </div>
    </CashierContext.Provider>

  )
}
