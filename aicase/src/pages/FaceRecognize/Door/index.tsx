import { useState, useEffect } from 'react'
import '../index.scss'
import Sidebar from '../../../components/Sidebar'

import Door_wait from '../../../assets/imgs/faceRecognize/Door/door_wait.png'
import Door_open_and_close from '../../../assets/imgs/faceRecognize/Door/door_open_and_close.gif'
import Door_err from '../../../assets/imgs/faceRecognize/Door/door_err.png'

import Sleep from '../../../utils/sleep';

export default function Door() {

  const [spotResult, setSpotResult] = useState('standby'); // 人脸检测结果

  const aisleHandle = async () => {
    switch(spotResult){
      case 'standby':
        break;
      case 'false':
        await Sleep(2000);
        setSpotResult('standby');
        break;
      case 'true':  
        await Sleep(6000);
        setSpotResult('standby');
    }
  }

  useEffect(()=>{
    aisleHandle();
  },[spotResult]);

  return (
    <div className='Door'>
      <Sidebar setSpotResult={setSpotResult} module='Door' text='人脸信息库'/>
      <div className="Door_scenes scenes">
        <div className="Door_img img">
          {
            spotResult === 'standby' ? <img src={Door_wait} alt="" className="Door_content content"/> : 
            spotResult === 'false' ? <img src={Door_err} alt="" className="Door_content content"/> : 
            spotResult === 'true' ? <img src={Door_open_and_close} alt="" className="Door_content content"/> : ''
          }
        </div>
      </div>
    </div>
  )
}
