import { useState, useEffect } from 'react'
import '../index.scss'
import Sleep from '../../../utils/sleep';
import PoliceSidebar from '../../../components/PoliceSidebar';
import Police_wait from '../../../assets/imgs/faceRecognize/Police/Police_wait.png'
import Police_success from '../../../assets/imgs/faceRecognize/Police/Police_success.gif'

export default function Police() {

    const [spotResult, setSpotResult] = useState('standby'); // 人脸检测结果

    const aisleHandle = async () => {
      switch(spotResult){
        case 'standby':
          break;
        case 'true':  
          await Sleep(3000);
          setSpotResult('standby');
      }
    }
  
    useEffect(()=>{
      aisleHandle();
    },[spotResult]);

  return (
    <div className='Police'>
        <PoliceSidebar setSpotResult={setSpotResult} module='Police' text='乘客图片'/>
        <div className="Cashier_scenes scenes">
        <div className="Cashier_img img">
            {
                spotResult === 'standby' ? <img src={Police_wait} alt="" className="Cashier_content content"/> : 
                spotResult === 'true' ? <img src={Police_success} alt="" className="Cashier_content content"/> : ''
            }
        </div>
        </div>
    </div>
  )
}
