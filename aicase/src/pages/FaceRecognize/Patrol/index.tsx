import { useState } from 'react'
import '../index.scss'
import PatrolSidebar from '../../../components/PatrolSidebar';
import Patrol_success from '../../../assets/imgs/faceRecognize/Patrol/Patrol_success.gif'
import Patrol_wait from '../../../assets/imgs/faceRecognize/Patrol/Patrol_wait.png'

export default function Patrol() {

    const [spotResult, setSpotResult] = useState('standby'); // 巡逻状态

  return (
    <div className='Patrol'>
        <PatrolSidebar setSpotResult={setSpotResult} spotResult={spotResult} module='Police' text='信息库'/>
        <div className="Cashier_scenes scenes">
        <div className="Cashier_img img">
            {
                spotResult === 'standby' ? <img src={Patrol_wait} alt="" className="Cashier_content content"/> : 
                spotResult === 'true' ? <img src={Patrol_success} alt="" className="Cashier_content content"/> : ''
            }
        </div>
        </div>
    </div>
  )
}