import { useState, useEffect } from 'react'
import '../index.scss'

import BarrierGateSidebar from '../../../components/BarrierGateSidebar';
import Sleep from '../../../utils/sleep';

import BarrierGate_open_and_close from '../../../assets/imgs/faceRecognize/BarrierGate/BarrierGate_open_and_close.gif'
import BarrierGate_wait from '../../../assets/imgs/faceRecognize/BarrierGate/BarrierGate_wait.png'
import BarrierGate_fail from '../../../assets/imgs/faceRecognize/BarrierGate/BarrierGate_fail.png'

export default function BarrierGate() {

    const [spotResult, setSpotResult] = useState('standby'); // 人脸识别结果

    const aisleHandle = async () => {
        switch(spotResult){
          case 'standby':
            break;
          case 'false':
            await Sleep(2000);
            setSpotResult('false');
            break;
          case 'true':
            await Sleep(3700);
            setSpotResult('standby');
        }
      }
    
      useEffect(()=>{
        aisleHandle();
      },[spotResult]);

  return (
    <div className='BarrierGate'>
        <BarrierGateSidebar setSpotResult={setSpotResult} module='BarrierGate'/>
        <div className="BarrierGate_scenes scenes">
        <div className="BarrierGate_img img">
            {
                spotResult === 'standby' ? <img src={BarrierGate_wait} alt="" className="BarrierGate_content content" /> :
                spotResult === 'true' ? <img src={BarrierGate_open_and_close} alt="" className="BarrierGate_content content" /> :
                spotResult === 'false' ? <img src={BarrierGate_fail} alt="" className="BarrierGate_content content" /> : ''
            }
        </div>
      </div>
    </div>
  )
}
