import { useState, useEffect } from 'react'
import '../index.scss'

import Sidebar from '../../../components/Sidebar';
import Sleep from '../../../utils/sleep';

import CheckIn_wait from '../../../assets/imgs/faceRecognize/CheckIn/CheckIn_wait.png'
import CheckIn_fail from '../../../assets/imgs/faceRecognize/CheckIn/CheckIn_fail.gif'
import CheckIn_success from '../../../assets/imgs/faceRecognize/CheckIn/CheckIn_success.gif'

export default function CheckIn() {
    const [spotResult, setSpotResult] = useState('standby'); // 人脸识别结果

    const aisleHandle = async () => {
        switch(spotResult){
          case 'standby':
            break;
          case 'false':
            await Sleep(3300);
            setSpotResult('standby');
            break;
          case 'true':
            await Sleep(3300);
            setSpotResult('standby');
        }
      }
    
      useEffect(()=>{
        aisleHandle();
      },[spotResult]);

  return (
    <div className='CheckIn'>
        <Sidebar setSpotResult={setSpotResult} module='CheckIn' text='人脸信息库'/>
        <div className="CheckIn_scenes scenes">
        <div className="CheckIn_img img">
            {
                spotResult === 'standby' ? <img src={CheckIn_wait} alt="" className="CheckIn_content content" /> :
                spotResult === 'true' ? <img src={CheckIn_success} alt="" className="CheckIn_content content" /> :
                spotResult === 'false' ? <img src={CheckIn_fail} alt="" className="CheckIn_content content" /> : ''
            }
        </div>
      </div>
    </div>
  )
}
