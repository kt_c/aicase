import React from 'react'
import './index.scss'
import HoverWord from '../../utils/HoverWord'

export default function Home() {

  return (
    <div className="Home">
      <HoverWord text='AI CASE' fontSize='50px' color='#b07f1e'/>
      <h1>人工智能案例动态仿真系统</h1>
    </div>
  )
}
