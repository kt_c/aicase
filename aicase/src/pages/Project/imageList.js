import project00 from '../../assets/imgs/projectFace/1/1.png'
import project01 from '../../assets/imgs/projectFace/1/2.jpg'
import project02 from '../../assets/imgs/projectFace/1/3.jpg'
import project03 from '../../assets/imgs/projectFace/1/4.jpg'
import project04 from '../../assets/imgs/projectFace/1/5.jpg'
import project05 from '../../assets/imgs/projectFace/1/6.jpg'
import project06 from '../../assets/imgs/projectFace/1/7.jpg'
import project07 from '../../assets/imgs/projectFace/1/8.jpg'
import project08 from '../../assets/imgs/projectFace/1/9.jpg'
import project09 from '../../assets/imgs/projectFace/1/91.jpg'
import project10 from '../../assets/imgs/projectFace/2/1.jpg'
import project11 from '../../assets/imgs/projectFace/2/2.jpg'
import project12 from '../../assets/imgs/projectFace/2/3.jpg'
import project13 from '../../assets/imgs/projectFace/2/4.jpg'
import project14 from '../../assets/imgs/projectFace/2/5.jpg'
import project15 from '../../assets/imgs/projectFace/2/6.jpg'
import project20 from '../../assets/imgs/projectFace/3/1.jpg'
import project21 from '../../assets/imgs/projectFace/3/2.jpg'
import project22 from '../../assets/imgs/projectFace/3/3.webp'
import project23 from '../../assets/imgs/projectFace/3/4.jpg'
import project24 from '../../assets/imgs/projectFace/3/5.webp'
import project25 from '../../assets/imgs/projectFace/3/6.jpg'
import project26 from '../../assets/imgs/projectFace/3/7.webp'
import project27 from '../../assets/imgs/projectFace/3/8.jpg'
import project28 from '../../assets/imgs/projectFace/3/9.webp'
import project29 from '../../assets/imgs/projectFace/3/91.webp'
import project30 from '../../assets/imgs/projectFace/4/1.jpg'
import project31 from '../../assets/imgs/projectFace/4/2.jpg'
import project32 from '../../assets/imgs/projectFace/4/3.jpg'
import project33 from '../../assets/imgs/projectFace/4/4.jpg'
import project34 from '../../assets/imgs/projectFace/4/5.jpg'
import project35 from '../../assets/imgs/projectFace/4/6.jpg'
import project36 from '../../assets/imgs/projectFace/4/7.jpg'
import project37 from '../../assets/imgs/projectFace/4/8.jpg'
import project38 from '../../assets/imgs/projectFace/4/9.jpg'
import project39 from '../../assets/imgs/projectFace/4/91.jpg'
import project40 from '../../assets/imgs/projectFace/5/1.jpg'
import project41 from '../../assets/imgs/projectFace/5/2.jpg'
import project42 from '../../assets/imgs/projectFace/5/3.jpg'
import project43 from '../../assets/imgs/projectFace/5/4.jpg'
import project44 from '../../assets/imgs/projectFace/5/5.jpg'
import project50 from '../../assets/imgs/projectFace/6/1.jpg'
import project51 from '../../assets/imgs/projectFace/6/2.jpeg'
import project52 from '../../assets/imgs/projectFace/6/3.jpeg'
import project53 from '../../assets/imgs/projectFace/6/4.jpeg'
import project54 from '../../assets/imgs/projectFace/6/5.jpeg'
import project55 from '../../assets/imgs/projectFace/6/6.jpeg'
import project60 from '../../assets/imgs/projectFace/7/1.jpeg'
import project61 from '../../assets/imgs/projectFace/7/2.jpeg'
import project62 from '../../assets/imgs/projectFace/7/3.jpeg'

const getAllimages = (type) => {
  // 在所有图片都成功导入后，可以在这里使用导入的图片
  let importedImages = []
  switch (type) {
    case 0:
      importedImages = [project00, project01, project02, project03, project04, project05, project06, project07, project08, project09]
      break; case 1:
      importedImages = [project10, project11, project12, project13, project14, project15]
      break; case 2:
      importedImages = [project20, project21, project22, project23, project24, project25, project26, project27, project28, project29]
      break; case 3:
      importedImages = [project30, project31, project32, project33, project34, project35, project36, project37, project38, project39]
      break; case 4:
      importedImages = [project40, project41, project42, project43, project44]
      break; case 5:
      importedImages = [project50, project51, project52, project53, project54, project55]
      break; case 6:
      importedImages = [project60, project61, project62]
      break; default:
      break
  }
  return importedImages
}
export default getAllimages
