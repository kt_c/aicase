import React, { useState, useRef, useEffect } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import './index.scss'
import { Card } from 'antd';
import { CaretRightOutlined, CaretLeftOutlined } from '@ant-design/icons';
const { Meta } = Card;
import { useLocation } from 'react-router-dom';
import Image from '../../components/Image'
//@ts-ignore
import getAllImages from "./imageList.js";//导入封面函数
import { v4 as uuidv4 } from 'uuid';
import turnstile_one from '../../assets/imgs/faceRecognize/turnstile/turnstile_one.png'


//轮播图
const getElement = () => { // 元素获取
  const slideRight = document.querySelector('[slide-index="slideRight"]') as HTMLDivElement
  const slide4 = document.querySelector('[slide-index="slide4"]') as HTMLDivElement
  const slide3 = document.querySelector('[slide-index="slide3"]') as HTMLDivElement
  const slide2 = document.querySelector('[slide-index="slide2"]') as HTMLDivElement
  const slide1 = document.querySelector('[slide-index="slide1"]') as HTMLDivElement
  const slide0 = document.querySelector('[slide-index="slide0"]') as HTMLDivElement
  const slideLeft = document.querySelector('[slide-index="slideLeft"]') as HTMLDivElement

  return { slideRight: slideRight, slide4: slide4, slide3: slide3, slide2: slide2, slide1: slide1, slide0: slide0, slideLeft: slideLeft }
}
const leftSilde = () => { // 左键   

  const { slideRight, slide4, slide3, slide2, slide1, slide0 } = getElement()

  if (!slide3) return

  slideRight ? slideRight.style.animation = 'move_right_to_4 1s' : ''
  slide4 ? slide4.style.animation = 'move_4_to_3 1s' : ''
  slide3.style.animation = 'move_3_to_2 1s'
  slide2.style.animation = 'move_2_to_1 1s'
  slide1 ? slide1.style.animation = 'move_1_to_0 1s' : ''
  slide0 ? slide0.style.animation = 'move_0_to_left 1s' : ''

  slideRight ? slideRight.setAttribute("slide-index", "slide4") : ''
  slide4 ? slide4.setAttribute("slide-index", "slide3") : ''
  slide3.setAttribute("slide-index", "slide2")
  slide2.setAttribute("slide-index", "slide1")
  slide1 ? slide1.setAttribute("slide-index", "slide0") : ''
  slide0 ? slide0.setAttribute("slide-index", "slideLeft") : ''
}
const rightSilde = () => { // 右键
  const { slide4, slide3, slide2, slide1, slide0, slideLeft } = getElement()

  if (!slide1) return

  slide4 ? slide4.style.animation = 'move_4_to_right 1s' : ''
  slide3 ? slide3.style.animation = 'move_3_to_4 1s' : ''
  slide2.style.animation = 'move_2_to_3 1s'
  slide1.style.animation = 'move_1_to_2 1s'
  slide0 ? slide0.style.animation = 'move_0_to_1 1s' : ''
  slideLeft ? slideLeft.style.animation = 'move_left_to_0 1s' : ''

  slideLeft ? slideLeft.setAttribute("slide-index", "slide0") : ''
  slide0 ? slide0.setAttribute("slide-index", "slide1") : ''
  slide1.setAttribute("slide-index", "slide2")
  slide2.setAttribute("slide-index", "slide3")
  slide3 ? slide3.setAttribute("slide-index", "slide4") : ''
  slide4 ? slide4.setAttribute("slide-index", "slideRight") : ''
}

export default function Case() {
  const navigate = useNavigate()
  const location = useLocation();
  const [scenes, setScenes] = useState<{ path: string; name: string; img: string; }[]>([]);
  //是否选择具体场景
  const [choosed, setChoosed] = useState<boolean>(false)

  useEffect(() => {
    // 在地址变化时执行的逻辑
    const currentPath = location.pathname;
    chooseProject(currentPath)

    // 执行其他操作或更新状态等
    // ...
  }, [location]);

  //根据路径判断项目 scenes path:场景路径 name:场景名称 img:场景照片
  const chooseProject = function (currentPath: string) {
    setChoosed(false)
    let count = 0
    let allImages = []
    let scenes: any = []
    switch (currentPath) {
      case '/faceRecognize'://人脸识别
        allImages = getAllImages(0)
        scenes = [{ path: 'Turnstile', name: '人脸识别闸机', img: turnstile_one }, { path: 'Door', name: '智能门', img: allImages[count++] },
        { path: 'Usher', name: '迎宾机器人', img: turnstile_one }, { path: 'Cashier', name: '自助收银机', img: turnstile_one },
        { path: 'Police', name: '电子警察', img: turnstile_one }, { path: 'CheckIn', name: '打卡机器人', img: turnstile_one },
        { path: 'Patrol', name: '巡逻机器人', img: turnstile_one }, { path: 'BarrierGate', name: '智能道闸', img: turnstile_one },
        { path: 'Turnstile', name: '监控跟踪', img: turnstile_one }, { path: 'Turnstile', name: '手势换脸', img: turnstile_one },]
        break;
      case '/imgRecognize'://图像识别
        allImages = getAllImages(1)
        scenes = [{ path: 'Turnstile', name: '手势识别', img: turnstile_one }, { path: 'Turnstile', name: '无人车', img: turnstile_one },
        { path: 'Turnstile', name: '分拣机器人', img: turnstile_one }, { path: 'Turnstile', name: '避障小车', img: turnstile_one },
        { path: 'Turnstile', name: '巡线小车', img: turnstile_one }, { path: 'Turnstile', name: '自助收银机', img: turnstile_one },
        ]
        break;
      case '/gestureRecognize'://手势识别
        allImages = getAllImages(2)
        scenes = [{ path: 'Turnstile', name: '手势识别', img: turnstile_one }, { path: 'Turnstile', name: '迎宾机器人', img: turnstile_one },
        { path: 'Turnstile', name: '智能窗帘', img: turnstile_one }, { path: 'Turnstile', name: '智能门', img: turnstile_one },
        { path: 'Turnstile', name: '智能小车', img: turnstile_one }, { path: 'Turnstile', name: '切水果', img: turnstile_one },
        { path: 'Turnstile', name: '智能垃圾桶', img: turnstile_one }, { path: 'Turnstile', name: '虚拟镜像', img: turnstile_one },
        { path: 'Turnstile', name: '虚拟VR', img: turnstile_one }, { path: 'Turnstile', name: '智能驾驶', img: turnstile_one },]
        break;
      case '/voiceRecognize'://语音识别
        allImages = getAllImages(3)
        scenes = [{ path: 'Turnstile', name: '迎宾机器人', img: turnstile_one }, { path: 'Turnstile', name: '智能窗帘', img: turnstile_one },
        { path: 'Turnstile', name: '智能台灯', img: turnstile_one }, { path: 'Turnstile', name: '智能小车', img: turnstile_one },
        { path: 'Turnstile', name: '车载系统', img: turnstile_one }, { path: 'Turnstile', name: '智能轮椅', img: turnstile_one },
        { path: 'Turnstile', name: '迎宾机器人2', img: turnstile_one }, { path: 'Turnstile', name: '陪伴机器人', img: turnstile_one },
        { path: 'Turnstile', name: '智能电视', img: turnstile_one }, { path: 'Turnstile', name: '智能冰箱', img: turnstile_one },]
        break;
      case '/naturalLanguage'://自然语言处理
        allImages = getAllImages(4)
        scenes = [{ path: 'Turnstile', name: '天气查询', img: turnstile_one }, { path: 'Turnstile', name: '音乐播放', img: turnstile_one },
        { path: 'Turnstile', name: '聊天机器人', img: turnstile_one }, { path: 'Turnstile', name: '校园客服', img: turnstile_one },
        { path: 'Turnstile', name: '语言理解', img: turnstile_one }
        ]
        break;
      case '/deepLearning'://深度学习
        allImages = getAllImages(5)
        scenes = [
          { path: 'licensePlate', name: '车牌识别', img: turnstile_one },
          { path: 'captcha', name: '数字验证码识别', img: turnstile_one },
          { path: 'GANimg', name: '使用GAN生成图片', img: turnstile_one },
          { path: 'ACGANimg', name: '使用ACGAN生成带标签的图片', img: turnstile_one },
          { path: 'Turnstile', name: '智能仪表识别', img: turnstile_one },
          { path: 'Turnstile', name: '斑马线识别', img: turnstile_one },
        ]
        break;
      case '/otherApp'://其他应用
        allImages = getAllImages(6)
        scenes = [{ path: 'Turnstile', name: '音乐推荐', img: turnstile_one }, { path: 'Turnstile', name: '图像标题生成', img: turnstile_one },
        { path: 'Turnstile', name: '边缘提取', img: turnstile_one }]
        break;
      default:
        setChoosed(true)
        break;
    }
    for (let i = 0; i < scenes.length; i++) {
      scenes[i].img = allImages[i]
    }
    setScenes(scenes)
  }
  const switchCase = (path: string) => { // 切换到具体的案例
    navigate(path)
  }

  return (
    <>{choosed ? <Outlet /> :
      <div className='Case'>
        <div className="swiper-container">
          <div className="left">
            <CaretLeftOutlined onClick={leftSilde} />
          </div>
          <div className="swiper-content">
            {scenes.length > 0 ? scenes.map((v: any, i: any) => {
              return (<Card
                key={uuidv4()}
                onClick={() => switchCase(v.path)}
                slide-index={scenes.length === 3 ? `slide${i + 1}` : i > 4 ? 'slideRight' : `slide${i}`}
                className='swiper-slide'
                hoverable
                style={{ width: '14rem' }}
                cover={<Image src={v.img ?? turnstile_one} />}
              >
                <div className='Card_content'><strong>{v.name}</strong></div>
              </Card>)
            }) : ''}
          </div>
          <div className="right">
            <CaretRightOutlined onClick={rightSilde} />
          </div>
        </div>
      </div>}</>
  )
}
