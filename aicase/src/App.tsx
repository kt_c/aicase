import { useState, createContext, useEffect } from 'react';
import { useRoutes } from 'react-router-dom'
import { ConfigProvider, Spin, Progress } from 'antd';
import './App.scss'
import Sleep from './utils/sleep';
import routes from './routes'
import Nav from './components/Nav'

export const AppContext = createContext<{
  setLoading: React.Dispatch<React.SetStateAction<boolean>>, // 一个全局的loading开关设置
  loading: boolean, // loading开关
  setProcessSwitch: React.Dispatch<React.SetStateAction<boolean>>, // 全局进度条开关
} | null>(null)

function App() {

  const element = useRoutes(routes)
  const [loading, setLoading] = useState(false); // loading开关
  const [percent, setPercent] = useState(0) // 进度条百分比
  const [processSwitch, setProcessSwitch] = useState(false) // 进度条开关

  const progressFormat = (percent?: number | undefined, successPercent?: number | undefined) => { // 进度条配置
    return (
      <div className='processContent'>
        {!percent ? '数据准备中' : percent < 25 ? '数据准备中' : percent < 50 ? '模型训练中' : percent < 75 ? '模型生成中' : percent < 100 ? '模型部署中' : percent == 100 ? '训练完成' : '模型准备中'}
      </div>
    )
  }

  const processChange = async () => { // 进度条变化
    if (processSwitch && percent != 100) {
      await Sleep(1)
      setPercent(percent + 1)
    }
    else if (percent == 100) {
      await Sleep(1000)
      setProcessSwitch(false)
      setPercent(0)
    }
  }

  useEffect(() => { // 进度条变化监听
    processChange()
  }, [processSwitch, percent])

  return (
    <AppContext.Provider value={{ 'setLoading': setLoading, 'loading': loading, 'setProcessSwitch': setProcessSwitch }}>
      <div className="App">
        <ConfigProvider theme={{ token: { colorPrimary: '#faad14' } }}>
          <Nav />
          <Spin spinning={loading}>
            <div className={`turnStile_mask ${processSwitch ? '' : 'hidden'}`}>
              <Progress percent={percent} size={300} type='circle' strokeColor={{ '0%': '#a0d911', '100%': '#faad14' }} format={progressFormat} />
            </div>
            {element}
          </Spin>
        </ConfigProvider>
      </div>
    </AppContext.Provider>
  )
}

export default App
