import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    chunkSizeWarningLimit: 1024, // 设置警告的块大小限制（以KB为单位）
    // 其他构建选项...
  },
})
